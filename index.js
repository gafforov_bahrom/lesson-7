let time = "00:00";
let x = 0; 
let isWorking = false;

function start() {
    x = parseInt(localStorage.getItem("time"), 10);
    if (isNaN(x)) {
        x = 0;
    }

    isWorking = true;
}

function stop() {
    isWorking = false;
}

function reset() {
    isWorking = false;
    time = "00:00";
    x = 0;
    localStorage.setItem("time", x);
    document.getElementById("watch").innerHTML = time;
}

function toString(x) {
    const seconds = x%60;
    const minutes = Math.floor(x / 60);
    if (minutes < 10) {
        time = "0"+minutes;
    } else {
        time = ""+minutes;
    }
    time += ":"
    if (seconds < 10) {
        time += "0"+seconds;
    } else {
        time += ""+seconds;
    }
    return time
}

function updateTimer() {
    x = parseInt(localStorage.getItem("time"), 10);
    if (isNaN(x)) {
        x = 0;
    }
    if (isWorking) {
        x++;
    }

    time = toString(x);
    document.getElementById("watch").innerHTML = time;
    localStorage.setItem("time", x);
    
}
updateTimer();
setInterval(updateTimer, 1000);