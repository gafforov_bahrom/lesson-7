# Lesson 7

Design UI, below

<img src="./task.png">

<br>

1. Your task is to design given task as it is. When clicking the button "START", it should start counting.<br>

2. If you click <code>Pause</code> button, it should save current state ( Count ) then save it to LocalStorage. Whenever the page is refreshed, it should continue from where it has stopped. <br>

3. When you click <code>Reset</code> button, state (Count) and LocalStorage should be cleaned.

